import styles from "./Product.module.scss";
type ProductType = {
  id: number;
  name: string;
  price: number;
  image: string;
  category: string;
};
const ProductView = ({ products }: { products: ProductType[] }) => {
  return (
    <div className={styles.product}>
      <h1 className={styles.product__title}>Product</h1>
      <div className={styles.product__content}>
        {products.map((product: ProductType) => (
          <div key={product.id} className={styles.product__content__item}>
            <div className={styles.product__content__item__image}>
              <img src={product.image} alt={product.name} />
            </div>
            <h1 className={styles.product__content__item__name}>
              {product.name}
            </h1>
            <p className={styles.product__content__item__category}>
              {product.category}
            </p>
            <p className={styles.product__content__item__price}>
              {product.price.toLocaleString("id-ID", {
                style: "currency",
                currency: "IDR",
              })}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ProductView;
