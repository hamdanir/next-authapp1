import { NextAuthOptions } from "next-auth";
import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials";

const authOption: NextAuthOptions = {
  session: {
    strategy: "jwt",
  },
  secret: process.env.NEXTAUTH_SECRET,
  providers: [
    CredentialsProvider({
      type: "credentials",
      name: "Credentials",
      credentials: {
        email: {
          label: "Email",
          type: "email",
          placeholder: "Email",
        },
        fullname: {
          label: "Fullname",
          type: "text",
          placeholder: "Fullname",
        },
        password: {
          label: "Password",
          type: "password",
          placeholder: "Password",
        },
      },
      async authorize(credentials) {
        const { email, password, fullname } = credentials as {
          email: string;
          fullname: string;
          password: string;
        };
        const user: any = {
          id: 1,
          email: email,
          password: password,
          fullname: fullname,
        };
        if (user) {
          return user;
        }
        return null;
      },
    }),
  ],
  callbacks: {
    jwt({ token, account, profile, user }: any) {
      if (account) {
        token.email = user.email;
        token.fullname = user.fullname;
      }
      return token;
    },

    async session({ session, token }: any) {
      if ("email" in token) {
        session.user.email = token.email;
      }
      if ("fullname" in token) {
        session.user.fullname = token.fullname;
      }
      return session;
    },
  },
};

export default NextAuth(authOption);
