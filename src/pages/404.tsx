import styles from "@/styles/404.module.scss";
const custom404 = () => {
  return (
    <div className={styles.error}>
      <img src="/404.png" alt="" className={styles.error__image} />
      <h1>Halaman Tidak Ditemukan | 404!</h1>
    </div>
  );
};

export default custom404;
