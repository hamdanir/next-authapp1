import LoginViews from "@/views/auth/login";

const LoginPage = () => {
  return (
    <>
      <LoginViews></LoginViews>
    </>
  );
};

export default LoginPage;
