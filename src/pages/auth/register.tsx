import Link from "next/link";
import styles from "./Register.module.css";

const RegisterPage = () => {
  return (
    <div className={styles.register}>
      <h1 className="text-3xl">Register Page</h1>
      <p>
        sudah punya akun? <Link href={"/auth/login"}>login</Link>
      </p>
    </div>
  );
};

export default RegisterPage;
