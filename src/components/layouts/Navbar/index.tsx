import styles from "@/components/layouts/Navbar/Navbar.module.css";
import { signIn, signOut, useSession } from "next-auth/react";
import Link from "next/link";
const Navbar = () => {
  const { data }: any = useSession();
  return (
    <div className={styles.navbar}>
      <div className={styles.nav_left}>
        <ul className={styles.nav_ul}>
          <li className={styles.li}>
            <Link href="/">Home</Link>
          </li>
          <li className={styles.li}>
            <Link href="/product">Product</Link>
          </li>
          <li className={styles.li}>
            <Link href="/about">About</Link>
          </li>
        </ul>
      </div>
      <div style={{ marginRight: "10px" }}>{data && data.user.fullname}</div>
      {data ? (
        <button onClick={() => signOut()}> Sign Out</button>
      ) : (
        <button onClick={() => signIn()}> Sign In</button>
      )}
    </div>
  );
};

export default Navbar;
